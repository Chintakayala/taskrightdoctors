//importing express module

const exp=require('express');

const app=exp();

//merging backend with angular

var path=require('path');

app.use(exp.static(path.join(__dirname,'../task/dist/task')));

//importing body parser

const bodyparser=require('body-parser');
app.use(bodyparser.json());

//importing peopleApi

const people=require('./routes/peoplesAPI');

app.use('/main',people)

//creating portnumber
var port=3000;
app.listen(port,()=>{
    console.log(`server listening on port ${port}`)
})