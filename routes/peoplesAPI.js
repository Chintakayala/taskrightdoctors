//creating mini express app

const exp=require('express');

const peopleApi=exp.Router();

//importing database connection

const connection=require('../DBconnection').connection();

const connObj=require('../DBconnection').connectionObject;

//req handler for registration

peopleApi.post('/register',(req,res)=>{
    dbo=connObj();
    dbo.collection('test1').insertOne(req.body,(err,success)=>{
        if(err){
            console.log('error while registering',err)
        }
        else{
            res.json({"message":"registered successfully"})
        }
    })
})

//req handler for getting details

peopleApi.get('/read',(req,res)=>{
    dbo=connObj();
    dbo.collection('test1').find({username:req.body.username}).toArray((err,resultArray)=>{
        if(err){
            console.log("error during Finding data",err)
        }
        else{
            res.json({"message":resultArray})
        }
    })
})

//req handler for update req

peopleApi.put('/update/:name',(req,res)=>{
    dbo=connObj();
    console.log(req.body);
    
    dbo.collection('test1').updateMany({ name: { $eq: req.params.name } },{$set:{
        name:req.body.name,
        age:req.body.age,
        gender:req.body.gender,
        mobile:req.body.mobile
    }},(err,success)=>{
        if(err){
            console.log("error while updating data",err)
        }
        else{
            res.json({"message":"Document updated successfully"})
        }
    })
})

//req handler for delete data

peopleApi.delete('/deleteuser/:name', (req, res) => {
    dbo=connObj();
    dbo.collection('test1').deleteOne({ name: { $eq: req.params.name } }, (err, success) => {
      if (err) {
        console.log("error while deleting",err)
      }
      else {
        dbo.collection('test1').find().toArray((err, dataArray) => {
          if (err) {
            console.log("error while deleting",err)
          }
          else {
            res.json({
              message: "record deleted",
              data: dataArray
            })
          }
        })
      }
    })
  })

module.exports=peopleApi;

